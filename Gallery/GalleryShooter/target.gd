extends Area2D

signal targetHit(targetVal, playerNum)

var velocity

# Called when the node enters the scene tree for the first time.
func _ready():
	print("Target created!")
	pass # Replace with function body.

func _process(delta):
	position += velocity * delta
	if (velocity.x > 0) && (get_viewport().size.x < (position.x - $TargetSprite.texture.get_width())):
		print("Target missed!")
		queue_free()
	elif (velocity.x < 0) && (0 > position.x):
		print("Target missed!")
		queue_free()

func _on_targetArea2D_area_shape_entered(area_id, area, area_shape, self_shape):
	print("Target Entered!")
	emit_signal("targetHit", 1, area.playerNum)
	queue_free()
