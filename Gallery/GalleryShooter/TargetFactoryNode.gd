extends Node

# MEMBER VARS
export(float) var targetScale = 1.0
export(float) var targetSpeed = 1.0
export(float) var targetSpawnTime = .5

export(Vector2) var spawnLocation
export(Vector2) var targetDirection

var active = false

var Target_Scene = preload("res://GalleryShooter/target.tscn")

func _ready():
	$SpawnTimer.wait_time = targetSpawnTime
	$SpawnTimer.start()
	print("Started spawn timer.")
	pass
	
func _process(delta):
	pass

func _on_SpawnTimer_timeout():
	print("New target.")
	var newTarget = Target_Scene.instance()
	newTarget.position = spawnLocation
	newTarget.velocity = targetDirection
	newTarget.connect("targetHit", get_node("../HUD"), "addScore")
	newTarget.scale = Vector2((5 + randi()%10) * 0.05, (5 + randi()%10) * 0.05)
	add_child(newTarget)



