extends CanvasLayer

export(Array, int) var score

# Called when the node enters the scene tree for the first time.
func _ready():
	score.clear()
	score.resize(2)
	score[0] = 0
	score[1] = 1
	pass # Replace with function body.

func addScore(targetVal, playerNum):
	score[playerNum] += targetVal
	if (playerNum == 0):
		$ScorePlayer1.text = str(score[0])
	elif (playerNum == 1):
		$ScorePlayer2.text = str(score[1])


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
