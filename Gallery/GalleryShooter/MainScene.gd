extends Node

# Declare member variables here.
var Bullet_Scene = preload("res://GalleryShooter/bullet.tscn")
var move_speed = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Handle player shooting.
func _input(event):
	if (event.is_action_pressed("player1_shoot")):
		var newBullet = Bullet_Scene.instance()
		newBullet.playerNum = 0
		newBullet.position = $Reticle1.position
		add_child(newBullet)
	if (event.is_action_pressed("player2_shoot")):
		var newBullet = Bullet_Scene.instance()
		newBullet.playerNum = 1
		newBullet.position = $Reticle2.position
		add_child(newBullet)

# Handle reticle movement.
func _process(delta):
	var ret1_pos = $Reticle1.position
	var ret2_pos = $Reticle2.position
	if (Input.is_action_pressed("player1_reticle_down")):
		ret1_pos.y += move_speed
	if (Input.is_action_pressed("player1_reticle_left")):
		ret1_pos.x -= move_speed
	if (Input.is_action_pressed("player1_reticle_up")):
		ret1_pos.y -= move_speed
	if (Input.is_action_pressed("player1_reticle_right")):
		ret1_pos.x += move_speed
		
	if (Input.is_action_pressed("player2_reticle_down")):
		ret2_pos.y += move_speed
	if (Input.is_action_pressed("player2_reticle_left")):
		ret2_pos.x -= move_speed
	if (Input.is_action_pressed("player2_reticle_up")):
		ret2_pos.y -= move_speed
	if (Input.is_action_pressed("player2_reticle_right")):
		ret2_pos.x += move_speed
	
	$Reticle1.position = ret1_pos
	$Reticle2.position = ret2_pos

# Not needed - can't remember how the signal gets picked up though.
#func onTargetHit(targetVal, playerNum):
#	$HUD.addScore(targetVal, playerNum)